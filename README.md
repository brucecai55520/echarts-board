# echarts-board

### 项目介绍
Echarts数据可视化大屏展示

### 主要目标
1. 可视化面板布局适配屏幕
2. 利用 ECharts 实现各类图表展示

### 核心技术
- 基于 flexible.js + rem 智能大屏适配
- VScode cssrem 插件
- Flex 布局
- Less 使用
- 基于 ECharts 数据可视化展示
- ECharts 配置地图